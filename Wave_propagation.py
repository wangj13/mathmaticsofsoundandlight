import scipy as sp
import scipy.signal as signal
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Wedge


# x_start = -1.1
# radius = origin_x - x_start

fig, axs = plt.subplots()

for i in np.arange(-5.0, 5.0, step=0.5):
    wavelength = 0.5
    origin_x, origin_y = -1.0, 0 + i
    max_radius = 6.0
    x_array = np.arange(origin_x, origin_x+max_radius, step=0.01)

    for x in x_array:
        radius = x - origin_x
        amplitude = radius % wavelength
        # amplitude = max(np.sin(amplitude / wavelength * 2 * np.pi), 0)
        amplitude = np.sin(amplitude / wavelength * 2 * np.pi)
        if (amplitude >= 0.99):
            semicircle = Wedge((origin_x, origin_y), radius, -90, 90, color='r', alpha=amplitude, lw=1, fill=False)
            axs.add_patch(semicircle)
            # amplitude = 0
        # print(amplitude)
        # circle = plt.Circle((origin_x, origin_y), radius, color='r', alpha=amplitude, lw=1, fill=False)
        # axs.add_patch(circle)


line_x = [-1.0, -1.0]
line_y = [-5.0, 5.0]
plt.plot(line_x, line_y, '-', color='black', linewidth=5)

# plt.axis('scaled')
plt.axis('equal')
plt.axis([-1.5, 5.0, -1.0, 1.0])
plt.show()
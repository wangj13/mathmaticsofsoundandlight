import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.animation as animation
import numpy as np

time_steps = 1000

# initialise points
x = 0
y = 6
amplitude = 2
k = 500
m = 50
c = 1
omega = np.sqrt(k / m)
zeta = c / (2 * np.sqrt(k * m))

ground = 0
spring_num = 8
spring_width = 0.5
rect_len = 2
rect_height = 1
damper_len = 0.2
damper_height = 1

y_position = []
time_sequence = []
x_position = []
# fig, axs = plt.subplots(1, 2, figsize=(12, 5),  gridspec_kw={'width_ratios': [1, 3]})
fig, axs = plt.subplots(1, 2, figsize=(12, 5))

marker_size = 30 #upped this to make points more visible

def animate(i):
    """ Perform animation step. """
    # calculate next y position
    y_next = amplitude * np.exp(-zeta * omega * i) * np.cos(np.sqrt(1 - zeta**2) * omega * i / 20) + y

    # prepare for drawing spring
    spring_x = [x]
    spring_y = [ground]
    Dis_SpingGround = y_next-0.5*rect_height - ground
    spring_per_len = abs(Dis_SpingGround) / spring_num

    for j in range(spring_num):
        spring_x.append(x + spring_width / 2.0)
        spring_x.append(x - spring_width / 2.0)
        spring_y.append(ground + j * spring_per_len + 0.25 * spring_per_len)
        spring_y.append(ground + j * spring_per_len + 0.75 * spring_per_len)

    spring_x.append(x)
    spring_y.append(y_next-0.5*rect_height)
    for j in range(len(spring_x)):
        spring_x[j] -= 0.4

    # prepare for drawing
    Dis_DamperGround = y_next - 0.5 * rect_height - ground

    #important - the figure is cleared and new axes are added
    fig.clear()
    axs[0] = fig.add_subplot(1, 3, 1)

    # draw rectangle and spring and damper
    rect_leftbottom = (x-0.5*rect_len, y_next-0.5*rect_height)
    rect = patches.Rectangle(rect_leftbottom, rect_len, rect_height, linewidth=1, edgecolor='r', facecolor='none')

    damper_leftbottom = (x + 0.4 - 0.5 * damper_len, 0.5 * Dis_DamperGround - 0.5 * damper_height)
    damper = patches.Rectangle(damper_leftbottom, damper_len, damper_height, linewidth=1, edgecolor='orange', facecolor='orange')
    damperLine_x = [x + 0.4, x + 0.4]
    damperLine_y = [ground, Dis_SpingGround + ground]
    # Add the patch to the Axes
    axs[0].add_patch(rect)
    axs[0].add_patch(damper)

    axs[0].plot(spring_x, spring_y)
    axs[0].plot(damperLine_x, damperLine_y, "-", "orange")

    #the new axes must be re-formatted
    axs[0].set_xlim(-2,2)
    axs[0].set_ylim(0,10)
    axs[0].set_xticks([])
    axs[0].set_yticks([])
    axs[0].text(0.02, 0.95, 'Time step = %d' % i, transform=axs[0].transAxes, fontsize=10)


    # draw amplitude vs time figure
    y_position.insert(0, y_next - y)
    for j in range(len(time_sequence)):
        time_sequence[j] += 1
    time_sequence.insert(0, 1)
    if len(time_sequence) > 100:
        del(time_sequence[-1])
        del(y_position[-1])


    axs[1] = fig.add_subplot(1, 3, (2, 3))
    # axs[1].set_xticks([])
    # axs[1].set_yticks([])
    axs[1].set_xlabel("Time")
    axs[1].set_ylabel("Amplitude")
    axs[1].set_xlim(0, 100)
    axs[1].set_ylim(0-y, 10-y)
    axs[1].plot(time_sequence, y_position)

ani = animation.FuncAnimation(fig, animate, interval=30, frames=range(time_steps))
plt.show()
# ani.save('TransverseWave.gif', writer='pillow')

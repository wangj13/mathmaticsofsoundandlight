import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

time_steps = 100

# initialise points
x = np.random.uniform(0, 4, 3000)
y = np.random.uniform(-1, 1, 3000)

fig, ax = plt.subplots(figsize=(12, 8))
marker_size = 30 #upped this to make points more visible

def animate(i):
    """ Perform animation step. """
    # calculate next y position
    y_next = y + 0.2 * np.sin(i / 100 * 2 * np.pi - x / 2.0 * 2 * np.pi)

    #important - the figure is cleared and new axes are added
    fig.clear()
    ax = fig.add_subplot(111)

    #the new axes must be re-formatted
    ax.set_title("Transverse wave simulation", fontsize=18)
    ax.set_xlim(0,4)
    ax.set_ylim(-2,2)
    ax.set_xticks([])
    ax.set_yticks([])
    # and the elements for this frame are added
    ax.text(0.02, 0.95, 'Time step = %d' % i, transform=ax.transAxes, fontsize=18)
    ax.scatter(x, y_next, s = marker_size, c = "black", cmap = "RdBu_r", marker = "o", edgecolor = None)

ani = animation.FuncAnimation(fig, animate, interval=1, frames=range(time_steps))
plt.show()
# ani.save('TransverseWave.gif', writer='pillow')

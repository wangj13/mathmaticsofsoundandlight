import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.animation as animation
import numpy as np

time_steps = 300

x_start, x_end = 0, 10
initial = 0
delta_x = 0.1
delta_t = 0.1
pluck_pos = 20  # how many delta from x_start
pluck_y = 2.0
c = 0.5
C = c * delta_t / delta_x  # pay attention, this C should not be too large (keep it less than about 1.1)
                           # otherwise, the simulation collapses

x_array = np.arange(x_start, x_end+delta_x, delta_x)
y_array = np.zeros(x_array.shape)  # initial is zero
y_array[pluck_pos] = pluck_y
pluck = (x_start + pluck_pos * delta_x, initial + pluck_y)

for i in range(pluck_pos):
    y_array[i] = pluck[1] / pluck[0] * (i * delta_x)

for i in range(1, (x_array.size) - pluck_pos):
    y_array[i + pluck_pos] = pluck[1] / (x_end - pluck[0]) * (x_end - (i + pluck_pos) * delta_x)

y_array_previous = y_array.copy()
y_array_current = y_array.copy()
y_array_temp = np.zeros(y_array_current.size)

fig, axs = plt.subplots(1, 2, figsize=(10, 5))

# marker_size = 30 #upped this to make points more visible

def animate(i):
    """ Perform animation step. """
    # calculate next y position

    for j in range(1, y_array_current.size - 1):
        y_array_temp[j] = -y_array_previous[j] + 2 * y_array_current[j] + C ** 2 * \
                          (y_array_current[j + 1] - 2 * y_array_current[j] + y_array_current[j - 1])

    for j in range(y_array_previous.size):
        y_array_previous[j] = y_array_current[j]
        y_array_current[j] = y_array_temp[j]

    fig.clear()
    axs[0] = fig.add_subplot(1, 1, 1)
    axs[0].set_xlim(0, 10)
    axs[0].set_ylim(-2.5, 2.5)
    axs[0].plot(x_array, y_array_current)
    axs[0].text(0.02, 0.95, 'Time step = %d' % i, transform=axs[0].transAxes, fontsize=10)


ani = animation.FuncAnimation(fig, animate, interval=30, frames=range(time_steps))
plt.show()

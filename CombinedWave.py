import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np

time_steps = 100

# initialise points
x = np.random.uniform(-5, 5, 5000)
y = np.random.uniform(-1, 1, 5000)

fig, ax = plt.subplots(figsize=(12, 5))
marker_size = 30 #upped this to make points more visible

def animate(i):
    """ Perform animation step. """
    # calculate next y position
    # if you want the points draw ellipticals, you should add a phase of 0.5*pi on one wave
    deepth_attenuation = ((y - (-1)) / 2) ** (2)
    y_next = y + deepth_attenuation * 0.2 * np.sin(i / 100 * 2 * np.pi - x / 2.0 * 2 * np.pi + 0.5 * np.pi)
    x_next = x + deepth_attenuation * 0.2 * np.sin(i / 100 * 2 * np.pi - x / 2.0 * 2 * np.pi)


    #important - the figure is cleared and new axes are added
    fig.clear()
    ax = fig.add_subplot(111)

    #the new axes must be re-formatted
    ax.set_title("Combined wave simulation", fontsize=18)
    ax.set_xlim(-4,4)
    ax.set_ylim(-1,2)
    ax.set_xticks([])
    ax.set_yticks([])
    # and the elements for this frame are added
    ax.text(0.02, 0.9, 'Time step = %d' % i, transform=ax.transAxes, fontsize=18)
    ax.scatter(x_next, y_next, s = marker_size, c = "black", cmap = "RdBu_r", marker = "o", edgecolor = None)

ani = animation.FuncAnimation(fig, animate, interval=1, frames=range(time_steps))
plt.show()
# ani.save('CombinedWave.gif', writer='pillow')

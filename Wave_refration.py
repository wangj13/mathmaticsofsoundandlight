import scipy as sp
import scipy.signal as signal
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Wedge


# x_start = -1.1
# radius = origin_x - x_start
line_x = []
line_y = []

fig, axs = plt.subplots()
wavelength = 0.5
max_radius = 7.0

for i in np.arange(-5.0, 5.0, step=0.5):
    origin_x, origin_y = -1.0, 0 + i
    line_x.append(origin_x)
    line_y.append(origin_y)
    x_array = np.arange(origin_x, origin_x+max_radius, step=0.01)

    for x in x_array:
        radius = x - origin_x
        amplitude = radius % wavelength
        # amplitude = max(np.sin(amplitude / wavelength * 2 * np.pi), 0)
        amplitude = np.sin(amplitude / wavelength * 2 * np.pi + origin_y / 3.0 * 2 * np.pi)
        if (amplitude >= 0.99):
            semicircle = Wedge((origin_x, origin_y), radius, -90, 90, color='r', alpha=amplitude, lw=1, fill=False)
            axs.add_patch(semicircle)
            # amplitude = 0
        # print(amplitude)
        # circle = plt.Circle((origin_x, origin_y), radius, color='r', alpha=amplitude, lw=1, fill=False)
        # axs.add_patch(circle)


for i in range(10):
    line_x_pre = [k - i*(wavelength / 1.0) for k in line_x]
    plt.plot(line_x_pre, line_y, '-', color='r', linewidth=2)


plt.plot(line_x, line_y, 'o-', markersize=10, color='black', linewidth=5)

# plt.axis('scaled')
plt.axis('equal')
plt.axis([-3.0, 5.0, -1.0, 1.0])
plt.show()